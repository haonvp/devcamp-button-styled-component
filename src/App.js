import ChildButton from "./components/ChildButton";
import StyledButton from "./components/StyledButton";
import StyledButtonWithCondition from "./components/StyledButtonWithCondition";
import StyledButtonWithProps from "./components/StyledButtonWithProps";
import SuperButton from "./components/SuperButton";

function App() {
  return (
    <div>
      <div>
        <h3>P1: </h3>
        <StyledButton>I am a styled button!</StyledButton>
      </div>
      <div>
        <h3>P2: </h3>
        <StyledButtonWithProps bg="yellow" color="red">Button 1</StyledButtonWithProps>
        <StyledButtonWithProps bg="purple" color="white">Button 2</StyledButtonWithProps>
        <StyledButtonWithProps bg="" color="">Button 3</StyledButtonWithProps>
      </div>
      <div>
        <h3>P3: </h3>
        <StyledButtonWithCondition type="primary">Primary button</StyledButtonWithCondition>
        <StyledButtonWithCondition>Normal button</StyledButtonWithCondition>
      </div>
      <div>
        <h3>P4: </h3>
        <SuperButton>Super button</SuperButton>
        <ChildButton bg="yellow">Button 1</ChildButton>
        <ChildButton bg="purple">Button 2</ChildButton>
        <ChildButton bg="pink">Button 3</ChildButton>
      </div>
    </div>
  );
}

export default App;
